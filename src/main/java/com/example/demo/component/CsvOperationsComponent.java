package com.example.demo.component;


import java.util.List;
import java.util.Map;

public interface CsvOperationsComponent {

    String PATH = "./src/resources/";

    List<String[]> readCsvFile(String fileName, String divider);
}

