package com.example.demo.service;

import com.example.demo.component.CsvOperationsComponent;
import com.example.demo.dto.ErrorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SudokuServiceImpl implements SudokuService {
    private final CsvOperationsComponent csvOperationsComponent;
    private ErrorDto error = new ErrorDto();

    @Autowired
    public SudokuServiceImpl(CsvOperationsComponent csvOperationsComponent) {
        this.csvOperationsComponent = csvOperationsComponent;
    }

    public ErrorDto readSudoku(String filename, String divider) {
        List<String[]> lines = csvOperationsComponent.readCsvFile(filename, divider);

        String[][] array_sudoku = new String[lines.size()][0];
        lines.toArray(array_sudoku);

        check(array_sudoku);
        return error;
    }

    public void checkRows(String[][] array_sudoku){
        for(int row = 0; row < 9; row++)
            for(int col = 0; col < 8; col++)
                for(int col2 = col + 1; col2 < 9; col2++) {
                    if (array_sudoku[row][col].equals(array_sudoku[row][col2])) {//wiersze
                        addRowsColsError(row,col2);
                    }
                    if (array_sudoku[col][row].equals(array_sudoku[col2][row])) {//kolumny
                        addRowsColsError(col2,row);
                    }
                }
    }

    public void checkBox(String[][] array_sudoku){//kwadraty
        for(int row = 0; row < 9; row += 3)
            for(int col = 0; col < 9; col += 3)
                for(int pos = 0; pos < 8; pos++)
                    for(int pos2 = pos + 1; pos2 < 9; pos2++)
                        if(array_sudoku[row + pos % 3][col + pos / 3].equals(array_sudoku[row + pos2 % 3][col + pos2 / 3])) {
                            addBoxError(row+col/3);
                            addRowsColsError(row + pos2 % 3,col + pos2 / 3);
                        }
    }

    public void check (String[][] array_sudoku){
        checkRows(array_sudoku);
        checkBox(array_sudoku);
    }

    public void addRowsColsError(int row, int col){
        if(!error.lineIds.contains(row) && !error.columnIds.contains(col)) {
            error.lineIds.add(row);
            error.columnIds.add(col);
        }
    }

    public void addBoxError(int box_id){
        if(!error.areaIds.contains(box_id)) {
            error.areaIds.add(box_id);
        }
    }

}
