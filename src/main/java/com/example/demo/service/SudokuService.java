package com.example.demo.service;


import com.example.demo.dto.ErrorDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface SudokuService {
    ErrorDto readSudoku(String filename, String divider);
    public void checkRows(String[][] array_sudoku);
    public void checkBox(String[][] array_sudoku);
    public void check (String[][] array_sudoku);
    public void addRowsColsError(int row, int col);
    public void addBoxError(int box_id);
}
