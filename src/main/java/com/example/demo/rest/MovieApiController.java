package com.example.demo.rest;

import com.example.demo.dto.ErrorDto;
import com.example.demo.service.SudokuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);
    @Autowired
    private SudokuService sudokuService;
    private List<String[]> lines = new ArrayList<>();

    @CrossOrigin
    @GetMapping("/sudoku")
    public ResponseEntity<ErrorDto> sudoku() {
        ErrorDto error = sudokuService.readSudoku("sudoku.csv", ";");
        if(error.columnIds.isEmpty() && error.lineIds.isEmpty() && error.areaIds.isEmpty()) {
            return ResponseEntity.ok().body(error);
        }
        else {
            return ResponseEntity.badRequest().body(error);
        }
    }
}


