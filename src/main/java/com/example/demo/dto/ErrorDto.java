package com.example.demo.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ErrorDto {
    public ArrayList<Integer> lineIds = new ArrayList<Integer>();
    public ArrayList<Integer> columnIds = new ArrayList<Integer>();
    public ArrayList<Integer> areaIds = new ArrayList<Integer>();


    @Override
    public String toString() {
        return "{  ErrorDto: ["+
                                "{ \n " +
                                "\"lineIds\": " + lineIds + ",\n"
                                + "\"columnIds\": \" + columnIds + ,\n"
                                + "\"areaIds\": " + areaIds + "\n"
                                + "}"
                + "] }";
    }
}
